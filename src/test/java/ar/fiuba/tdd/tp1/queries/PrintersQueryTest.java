package ar.fiuba.tdd.tp1.queries;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.formulas.formulacreator.FormulaCreator;
import ar.fiuba.tdd.tp1.printers.console.ConsoleBookPrinter;
import ar.fiuba.tdd.tp1.printers.csv.CSVBookPrinter;
import ar.fiuba.tdd.tp1.printers.json.JsonBookPrinter;
import ar.fiuba.tdd.tp1.queries.factory.QueryFactory;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertThat;


/**
 * Created by maxi on 22/10/15.
 */
public class PrintersQueryTest {

    public static final String NEW_SHEET = "New sheet";

    private Book book;

    @Before
    public void setUpStreams() {
        book = new Book();
        book.createSheet(NEW_SHEET);
    }

    @Test
    public void executeConsolePrinterQuery() {

        Sheet sheet = book.getSheet(NEW_SHEET);
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        Query consolePrinterQuery = QueryFactory.printQuery().activeSheetToConsole("A", 1);
        consolePrinterQuery.setContext(book);
        assertThat(consolePrinterQuery.execute(), instanceOf(ConsoleBookPrinter.class));
    }

    @Test
    public void executeJsonPrinterQuery() {

        Sheet sheet = book.getSheet(NEW_SHEET);
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        Query jsonPrinterQuery = QueryFactory.printQuery().bookToFile("fileName");
        assertThat(jsonPrinterQuery.execute(), instanceOf(JsonBookPrinter.class));
    }

    @Test
    public void executeCSVPrinterQuery() {

        Sheet sheet = book.getSheet(NEW_SHEET);
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        Query sheetCsvPrinteQuery = QueryFactory.printQuery().activeSheetToCSV("fileName");
        assertThat(sheetCsvPrinteQuery.execute(), instanceOf(CSVBookPrinter.class));
    }
}
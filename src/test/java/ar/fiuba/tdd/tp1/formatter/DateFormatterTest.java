package ar.fiuba.tdd.tp1.formatter;

import ar.fiuba.tdd.tp1.elements.formatter.FormatException;
import ar.fiuba.tdd.tp1.elements.formatter.Formatter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by jonathan on 14/10/15.
 */
public class DateFormatterTest {

    @Test(expected = FormatException.class)
    public void formatNoDate() throws Exception {
        new DateFormatter(DateFormat.DDMMYYYY).format("Aaaaaa");
    }


    @Test
    public void formatDayMonthYear() throws Exception {
        Formatter fm = new DateFormatter(DateFormat.DDMMYYYY);
        String result = fm.format("10-20-2015");
        assertEquals("20-10-2015", result);
    }

    @Test
    public void formatMonthDayYear() throws Exception {
        Formatter fm = new DateFormatter(DateFormat.MMDDYYYY);
        String result = fm.format("2015-10-22");
        assertEquals(fm.getType(), "DATE");
        assertEquals(fm.getFormat(), "MMDDYYYY");
        assertEquals("10-22-2015", result);
    }


    @Test
    public void formatYearMonthDay() throws Exception {
        Formatter fm = new DateFormatter(DateFormat.YYYYMMDD);
        String result = fm.format("10-22-2015");
        assertEquals("2015-10-22", result);
    }


    @Test(expected = FormatException.class)
    public void wrongInputFormat() throws Exception {
        Formatter fm = new DateFormatter(DateFormat.YYYYMMDD);
        String result = fm.format("AAAAAAA");
    }


}

package ar.fiuba.tdd.tp1.formatter;

import ar.fiuba.tdd.tp1.elements.formatter.FormatException;
import ar.fiuba.tdd.tp1.elements.formatter.Formatter;
import org.junit.Test;

import java.text.DecimalFormat;

import static org.junit.Assert.assertEquals;

/**
 * Created by jonathan on 14/10/15.
 */
public class NumberFormatterTest {

    private static final char SEPARATOR = '.';

    @Test
    public void noDecimalsFormat() throws Exception {
        Formatter fm = new NumberFormatter(0);
        String result = fm.format("200.899");
        assertEquals("201", result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void negativeDecimals() {
        Formatter fm = new NumberFormatter(-1);
        fm.format("200.33333");
    }

    @Test
    public void positiveDecimals() {
        Formatter fm = new NumberFormatter(3);
        String result = fm.format("200.33");
        assertEquals("200" + SEPARATOR + "330", result);
    }

    @Test(expected = FormatException.class)
    public void formatNoNumber() {
        Formatter fm = new NumberFormatter(3);
        fm.format("20a0.33");

    }
}

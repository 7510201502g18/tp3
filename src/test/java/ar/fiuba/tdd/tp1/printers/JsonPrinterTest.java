package ar.fiuba.tdd.tp1.printers;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Cell;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.formatter.CurrencySymbol;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;
import ar.fiuba.tdd.tp1.formulas.formulacreator.FormulaCreator;
import ar.fiuba.tdd.tp1.printers.json.JsonBookPrinter;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by maxi on 12/10/15.
 */
public class JsonPrinterTest {


    private static final String FILE_NAME = "aJson";
    private static final String FILE_EXTENSION = ".jss";
    private static final String FILE_PATH = File.separator + "tmp" + File.separator + FILE_NAME + FILE_EXTENSION;
    private static final String PRINT_NAME = File.separator + "tmp" + File.separator + FILE_NAME;

    @Test
    public void createJsonWithOneCell() {
        Book book = new Book();
        book.createSheet("New sheet");
        Sheet sheet = book.getSheet("New sheet");
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        JsonBookPrinter jsonPrinter = new JsonBookPrinter(book, PRINT_NAME);
        jsonPrinter.print();
        File file = new File(FILE_PATH);
        Assert.assertTrue(file.canRead());
        String expectedResult = "{\"name\":\"default\",";
        expectedResult += "\"cells\":[{\"sheet\":\"New sheet\",\"id\":\"A1\",\"value\":\"1.0\",";
        expectedResult += "\"formatter\":{\"type\":\"IDENTITY\",\"format\":\"\"}}]}";
        try {
            Assert.assertEquals(expectedResult, new Scanner(file).useDelimiter("\\A").next());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createJsonWithMultiplesCells() {
        Book book = new Book();
        book.createSheet("New sheet");
        Sheet sheet = book.getSheet("New sheet");
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        sheet.getCell("B", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        JsonBookPrinter jsonPrinter = new JsonBookPrinter(book, PRINT_NAME);
        jsonPrinter.print();
        File file = new File(FILE_PATH);
        Assert.assertTrue(file.canRead());
        String expectedResult = "{\"name\":\"default\",";
        expectedResult += "\"cells\":[{\"sheet\":\"New sheet\",\"id\":\"A1\",\"value\":\"1.0\",";
        expectedResult += "\"formatter\":{\"type\":\"IDENTITY\",\"format\":\"\"}},";
        expectedResult += "{\"sheet\":\"New sheet\",\"id\":\"B1\",\"value\":\"1.0\",";
        expectedResult += "\"formatter\":{\"type\":\"IDENTITY\",\"format\":\"\"}}]}";
        try {
            Assert.assertEquals(expectedResult, new Scanner(file).useDelimiter("\\A").next());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createJsonWithMultiplesSheets() throws FileNotFoundException {

        Book book = new Book();
        book.createSheet("New sheet");
        Sheet sheet = book.getSheet("New sheet");
        sheet.getCell("A", 1).setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        book.createSheet("Second sheet");
        Sheet secondSheet = book.getSheet("Second sheet");
        Cell cellA1 = secondSheet.getCell("A", 1);
        cellA1.setFormula(FormulaCreator.createFormulaFromInfix("1", book));
        cellA1.setFormatter(MoneyFormatter.twoDecimalsFormatter(CurrencySymbol.AR));
        JsonBookPrinter jsonPrinter = new JsonBookPrinter(book, PRINT_NAME);
        jsonPrinter.print();
        File file = new File(FILE_PATH);
        Assert.assertTrue(file.canRead());
        String expectedResult = "{\"name\":\"default\",";
        expectedResult += "\"cells\":[{\"sheet\":\"Second sheet\",\"id\":\"A1\",\"value\":\"1.0\",";
        expectedResult += "\"formatter\":{\"type\":\"MONEY\",\"format\":\"AR!2\"}},";
        expectedResult += "{\"sheet\":\"New sheet\",\"id\":\"A1\",\"value\":\"1.0\",";
        expectedResult += "\"formatter\":{\"type\":\"IDENTITY\",\"format\":\"\"}}]}";
        Assert.assertEquals(expectedResult, new Scanner(file).useDelimiter("\\A").next());
    }


}

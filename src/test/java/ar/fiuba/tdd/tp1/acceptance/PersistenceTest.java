package ar.fiuba.tdd.tp1.acceptance;

import ar.fiuba.tdd.tp1.acceptance.driver.ConcreteSpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PersistenceTest {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        this.testDriver = new ConcreteSpreadSheetTestDriver();
    }

    @Test
    public void persistOneWorkBookAndRetrieveIt() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "=10");
        testDriver.setCellValue("tecnicas", "default", "A2", "=default!A1 + 1");
        testDriver.setCellValue("tecnicas", "default", "A3", "=default!A2 + 1");
        testDriver.setCellValue("tecnicas", "default", "A4", "=default!A3 + 1");
        testDriver.setCellValue("tecnicas", "default", "A5", "=default!A4 + 1");
        testDriver.persistWorkBook("tecnicas", "proy1");
        testDriver.setCellValue("tecnicas", "default", "A1", "");
        testDriver.setCellValue("tecnicas", "default", "A2", "");
        testDriver.setCellValue("tecnicas", "default", "A3", "");
        testDriver.reloadPersistedWorkBook("proy1.jss");

        assertEquals(10 + 1 + 1 + 1 + 1, testDriver.getCellValueAsDouble("tecnicas", "default", "A5"), DELTA);
    }

    @Test
    public void persistCurrentSheetAsCsv() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.createNewWorkSheetNamed("tecnicas", "Sheet_1");
        testDriver.createNewWorkSheetNamed("tecnicas", "Sheet_2");
        testDriver.setCellValue("tecnicas", "Sheet_1", "A1", "=10");
        testDriver.setCellValue("tecnicas", "Sheet_1", "A2", "= Sheet_1!A1 + 1");
        testDriver.setCellValue("tecnicas", "Sheet_1", "A3", "=Sheet_1!A2 + 1");
        testDriver.setCellValue("tecnicas", "Sheet_2", "A4", "232");
        testDriver.saveAsCSV("tecnicas", "Sheet_1", "sheet1.csv");

        testDriver.loadFromCSV("tecnicas", "default2", "sheet1.csv");

        testDriver.setCellValue("tecnicas", "default2", "A2", "5");

        assertEquals(4, testDriver.sheetCountFor("tecnicas"));
        assertEquals("10.0", testDriver.getCellValueAsString("tecnicas", "default2", "A1"));
        assertEquals("5.0", testDriver.getCellValueAsString("tecnicas", "default2", "A2"));
        assertEquals("12.0", testDriver.getCellValueAsString("tecnicas", "default2", "A3"));
    }

}


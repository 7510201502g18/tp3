package ar.fiuba.tdd.tp1.formulas.formulacreator;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.sheet.CreateSheetAction;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import org.junit.Before;
import org.junit.Test;

import java.util.Stack;

import static org.junit.Assert.assertEquals;

/**
 * Created by jonathan on 3/10/15.
 */
public class ReferenceHandlerTest {


    private static final String SHEET_ID = "SHEET1";
    private static final String REFERENCE = SHEET_ID + "!A11";
    private Book book;

    @Before
    public void before() {
        book = new Book();
        Action sheetAction = new CreateSheetAction(SHEET_ID);
        sheetAction.setContext(book);
        sheetAction.execute();
    }

    @Test
    public void handle() {
        Stack<Formula> formulaTree = new Stack<>();
        ReferenceHandler handler = new ReferenceHandler(formulaTree, book);
        handler.handle(REFERENCE);
        assertEquals(1, formulaTree.size());
    }

    @Test
    public void notHandle() {
        Stack<Formula> formulaTree = new Stack<>();
        ReferenceHandler handler = new ReferenceHandler(formulaTree, book);
        handler.handle("3");
        assertEquals(0, formulaTree.size());
    }
}

package ar.fiuba.tdd.tp1.formulas;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.actions.sheet.CreateSheetAction;
import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.CyclicReferenceException;
import ar.fiuba.tdd.tp1.queries.Query;
import ar.fiuba.tdd.tp1.queries.cell.CellValueQuery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * ReferenceFormula class tests
 * Created by jonathan on 3/10/15.
 */
public class ReferenceFormulaTest {

    public static final String SHEET_1 = "sheet1";
    private Book book;


    @Before
    public void before() {
        book = new Book();
        Action createSheet = new CreateSheetAction(SHEET_1);
        createSheet.setContext(book);
        createSheet.execute();
    }

    @Test(expected = InvalidFormulaException.class)
    public void referenceInexistentSheet() {
        ReferenceFormula referenceFormula = new ReferenceFormula("HOJA1!A3", book);
        referenceFormula.evaluate();
    }

    @Test
    public void referenceCell() {
        final ReferenceFormula referenceFormula = new ReferenceFormula(SHEET_1 + "!A12", book);
        String value = referenceFormula.evaluate();
        Assert.assertEquals("0.0", value);
    }

    @Test(expected = InvalidFormulaException.class)
    public void badReference() {
        final ReferenceFormula referenceFormula = new ReferenceFormula(SHEET_1 + "!12", book);
        referenceFormula.evaluate();
    }

    @Test
    public void toStringReferenceFormula() {
        String referenceString = SHEET_1 + "!A12";
        final ReferenceFormula referenceFormula = new ReferenceFormula(referenceString, book);
        Assert.assertEquals(referenceString, referenceFormula.toString());
    }

    @Test(expected = CyclicReferenceException.class)
    public void basicCyclicReferenceTest() {
        Action firstAction = ActionFactory.cellAction("A", 1, SHEET_1).changeFormula("= sheet1!A2");
        firstAction.setContext(book);
        Action secondAction = ActionFactory.cellAction("A", 2, SHEET_1).changeFormula("= sheet1!A1");
        secondAction.setContext(book);
        firstAction.execute();
        secondAction.execute();

        Query<String> consulta = new CellValueQuery(SHEET_1, "A", 1);
        consulta.setContext(book);
        consulta.execute();

    }

    @Test
    public void complexReferenceTestNonCyclic() {
        Action firstAction = ActionFactory.cellAction("A", 1, SHEET_1).changeFormula("= sheet1!A2 + sheet1!A3");
        firstAction.setContext(book);
        Action secondAction = ActionFactory.cellAction("A", 2, SHEET_1).changeFormula("= sheet1!A4 + sheet1!A5");
        secondAction.setContext(book);
        Action thirdAction = ActionFactory.cellAction("A", 3, SHEET_1).changeFormula("= sheet1!A5");
        thirdAction.setContext(book);
        Action fourthAction = ActionFactory.cellAction("A", 4, SHEET_1).changeFormula("4");
        fourthAction.setContext(book);
        Action fifthAction = ActionFactory.cellAction("A", 5, SHEET_1).changeFormula("5");
        fifthAction.setContext(book);
        firstAction.execute();
        secondAction.execute();
        thirdAction.execute();
        fourthAction.execute();
        fifthAction.execute();

        Query<String> consulta = new CellValueQuery(SHEET_1, "A", 1);
        consulta.setContext(book);
        Assert.assertEquals("14.0", consulta.execute());
    }

    @Test(expected = CyclicReferenceException.class)
    public void complexReferenceTestCyclic() {
        Action firstAction = ActionFactory.cellAction("A", 1, SHEET_1).changeFormula("= sheet1!A2 + sheet1!A3");
        firstAction.setContext(book);
        Action secondAction = ActionFactory.cellAction("A", 2, SHEET_1).changeFormula("= sheet1!A4 + sheet1!A5");
        secondAction.setContext(book);
        Action thirdAction = ActionFactory.cellAction("A", 3, SHEET_1).changeFormula("= sheet1!A2");
        thirdAction.setContext(book);
        Action fourthAction = ActionFactory.cellAction("A", 4, SHEET_1).changeFormula("4");
        fourthAction.setContext(book);
        Action fifthAction = ActionFactory.cellAction("A", 5, SHEET_1).changeFormula("= sheet1!A3");
        fifthAction.setContext(book);
        firstAction.execute();
        secondAction.execute();
        thirdAction.execute();
        fourthAction.execute();
        fifthAction.execute();

        Query<String> consulta = new CellValueQuery(SHEET_1, "A", 1);
        consulta.setContext(book);
        consulta.execute();
    }

    @Test(expected = CyclicReferenceException.class)
    public void abcCyclicTest() {
        Action first = ActionFactory.cellAction("A", 1, SHEET_1).changeFormula("= sheet1!A2");
        first.setContext(book);
        first.execute();
        Action second = ActionFactory.cellAction("A", 2, SHEET_1).changeFormula("= sheet1!A3");
        second.setContext(book);
        second.execute();
        Action third = ActionFactory.cellAction("A", 3, SHEET_1).changeFormula("= sheet1!A1");
        third.setContext(book);
        third.execute();

        Query<String> consulta = new CellValueQuery(SHEET_1, "A", 1);
        consulta.setContext(book);
        consulta.execute();
    }
}

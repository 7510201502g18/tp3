package ar.fiuba.tdd.tp1.formulas;

import ar.fiuba.tdd.tp1.elements.cell.Formula;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by matias on 9/25/15.
 */
public class NumberFormulaTest {

    @Test
    public void getNumberValue() {
        Formula numberFormula = new NumberFormula(2.0f);
        Assert.assertEquals(numberFormula.evaluate(), "2.0");
    }

    @Test
    public void getStringValue() {
        Formula numberFormula = new NumberFormula(2.0f);
        Assert.assertEquals(numberFormula.toString(), "2.0");
    }
}

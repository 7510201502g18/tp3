package ar.fiuba.tdd.tp1.queries.print;

import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.printers.console.ConsoleBookPrinter;
import ar.fiuba.tdd.tp1.queries.PrinterQuery;

/**
 * Created by jonathan on 15/10/15.
 */
public class ConsolePrinterQuery extends PrinterQuery {

    private String startingCol;
    private Integer startingRow;

    public ConsolePrinterQuery(String startingCol, Integer stratingRow) {
        this.startingCol = startingCol;
        this.startingRow = stratingRow;
    }

    @Override
    protected Printer createPrinter() {
        return new ConsoleBookPrinter(getContext(), this.startingCol, this.startingRow);
    }
}

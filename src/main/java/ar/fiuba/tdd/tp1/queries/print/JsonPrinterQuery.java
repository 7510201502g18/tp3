package ar.fiuba.tdd.tp1.queries.print;

import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.printers.json.JsonBookPrinter;
import ar.fiuba.tdd.tp1.queries.PrinterQuery;

/**
 * Created by jonathan on 15/10/15.
 */
public class JsonPrinterQuery extends PrinterQuery {

    private String fileName;

    public JsonPrinterQuery(String fileName) {
        this.fileName = fileName;
    }

    @Override
    protected Printer createPrinter() {
        return new JsonBookPrinter(getContext(), fileName);
    }
}

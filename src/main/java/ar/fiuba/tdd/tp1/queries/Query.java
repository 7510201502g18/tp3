package ar.fiuba.tdd.tp1.queries;

import ar.fiuba.tdd.tp1.elements.book.Book;

/**
 * Queries to elements.
 * Created by jonathan on 3/10/15.
 */
public abstract class Query<T> {

    private Book context;

    protected Book getContext() {
        return context;
    }

    public void setContext(Book context) {
        this.context = context;
    }

    public abstract T execute();
}

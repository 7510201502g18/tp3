package ar.fiuba.tdd.tp1.queries.cell;

import ar.fiuba.tdd.tp1.queries.CellQuery;

/**
 * Query that gets the formula that is beeing evaluated in the given cell.
 * Created by matias on 10/4/15.
 */
public class CellFormulaQuery extends CellQuery<String> {

    public CellFormulaQuery(String sheetId, String col, Integer row) {
        super(sheetId, col, row);
    }

    @Override
    public String execute() {
        return this.getCell().getFormula().toStringAsRootFormula();
    }

}

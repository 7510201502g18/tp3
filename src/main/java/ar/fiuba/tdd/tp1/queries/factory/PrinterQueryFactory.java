package ar.fiuba.tdd.tp1.queries.factory;

import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.queries.Query;

/**
 * Define the methods to be implemented to create book actions
 * Created by jonathan on 28/09/15.
 */
public interface PrinterQueryFactory {

    Query<Printer> activeSheetToCSV(String filename);

    Query<Printer> bookToFile(String filename);

    Query<Printer> activeSheetToConsole(String startingCol, Integer startingRow);
}

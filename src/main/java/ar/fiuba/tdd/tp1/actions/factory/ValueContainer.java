package ar.fiuba.tdd.tp1.actions.factory;

/**
 * Simple containe for a valuer used by the ACtionFactory.
 * Created by jonathan on 28/09/15.
 */
public class ValueContainer {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

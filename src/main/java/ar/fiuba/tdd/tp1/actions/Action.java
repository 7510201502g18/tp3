package ar.fiuba.tdd.tp1.actions;

import ar.fiuba.tdd.tp1.api.IllegalActionException;
import ar.fiuba.tdd.tp1.elements.book.Book;

/**
 * Generic action that can be executed on a sheet, book or cell.
 * Created by jonathan on 23/09/15.
 */
public abstract class Action {
    private Book context;
    private Boolean done = false;

    protected Book getContext() {
        return this.context;
    }

    public void setContext(Book book) {
        this.context = book;
        this.done = false;
    }

    public void execute() {
        if (done || context == null) {
            throw new IllegalActionException();
        }
        done = true;
    }


    public void undo() {
        if (!done) {
            throw new IllegalActionException();
        }
        done = false;
    }
}

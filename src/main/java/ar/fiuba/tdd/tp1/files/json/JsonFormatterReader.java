package ar.fiuba.tdd.tp1.files.json;

import ar.fiuba.tdd.tp1.actions.factory.CellActionFactory;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.elements.formatter.FormatException;
import ar.fiuba.tdd.tp1.elements.formatter.FormatInterpreter;
import ar.fiuba.tdd.tp1.elements.formatter.IdentityFormatter;
import ar.fiuba.tdd.tp1.formatter.DateFormatter;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;
import ar.fiuba.tdd.tp1.formatter.NumberFormatter;
import ar.fiuba.tdd.tp1.printers.json.model.JsonFormatter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jonathan on 18/10/15.
 */
public class JsonFormatterReader {

    private static final Map<String, FormatInterpreter> formatters = new HashMap<>();

    static {
        formatters.put(DateFormatter.TYPE, DateFormatter.interpreter());
        formatters.put(IdentityFormatter.TYPE, IdentityFormatter.interpreter());
        formatters.put(MoneyFormatter.TYPE, MoneyFormatter.interpreter());
        formatters.put(NumberFormatter.TYPE, NumberFormatter.interpreter());
    }

    public static void readFormatter(String bookName, CellActionFactory factory, JsonFormatter json, SpreadSheet api) {
        FormatInterpreter interpreter = formatters.get(json.getType());
        if (interpreter == null) {
            throw new FormatException("Wrong formatter type");
        }
        api.execute(bookName, factory.changeFormatter(interpreter.interpret(json.getFormat())));
    }
}

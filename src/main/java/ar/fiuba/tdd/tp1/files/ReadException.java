package ar.fiuba.tdd.tp1.files;

/**
 * Created by jonathan on 17/10/15.
 */
public class ReadException extends RuntimeException {

    public ReadException(String message) {
        super(message);
    }

    public ReadException(String message, Throwable cause) {
        super(message, cause);
    }
}

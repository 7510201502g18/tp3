package ar.fiuba.tdd.tp1.files.json;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.files.ReadException;
import ar.fiuba.tdd.tp1.printers.json.model.JsonBook;
import ar.fiuba.tdd.tp1.printers.json.model.JsonCell;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Created by jonathan on 17/10/15.
 */
public class JsonBookReader {

    public static void openBook(String fileName, SpreadSheet api) {
        JsonBook json = readJson(fileName);
        String bookName = json.getName();
        closePreviousBook(bookName, api);
        final List<JsonCell> cells = json.getCells();
        if (cells.size() == 0) {
            api.createBookWithDefaultSheet(bookName);

        } else {
            api.createBook(bookName);
        }
        JsonCellReader.createCells(bookName, cells, api);

    }

    private static void closePreviousBook(String bookName, SpreadSheet api) {
        if (api.getBooks().contains(bookName)) {
            api.closeBook(bookName);
        }
    }

    private static JsonBook readJson(String fileName) {
        ObjectMapper mapper = new ObjectMapper();
        try (InputStreamReader file = new InputStreamReader(new FileInputStream(fileName), "UTF-8")) {
            return mapper.readValue(file, JsonBook.class);
        } catch (IOException e) {
            throw new ReadException("Cannot open file " + fileName, e);
        } catch (RuntimeException e) {
            throw new ReadException("File " + fileName + " is corrupt", e);
        }
    }


}

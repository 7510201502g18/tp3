package ar.fiuba.tdd.tp1.elements.printer;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;

import java.util.Iterator;

/**
 * Created by jonathan on 10/10/15.
 */
public abstract class BookPrinter implements Printer {

    protected Book book;

    public BookPrinter(Book book) {
        this.book = book;
    }

    protected abstract void printHeader(Book book);

    protected Iterator<Sheet> sheetIterator() {
        return this.book.iterator();
    }

    protected abstract Printer getSheetPrinter(Sheet sheet);

    protected abstract void printSheetSeparator();

    protected abstract void endPrint();

    public void print() {
        printHeader(this.book);
        Iterator<Sheet> sheets = sheetIterator();
        Printer sheetPrinter;
        while (sheets.hasNext()) {
            sheetPrinter = getSheetPrinter(sheets.next());
            sheetPrinter.print();
            if (sheets.hasNext()) {
                printSheetSeparator();
            }
        }
        endPrint();
    }
}
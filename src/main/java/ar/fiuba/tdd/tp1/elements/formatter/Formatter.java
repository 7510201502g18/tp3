package ar.fiuba.tdd.tp1.elements.formatter;

/**
 * Created by jonathan on 14/10/15.
 */
public interface Formatter {
    String format(String value);

    String getType();

    String getFormat();

    void changeProperty(String property, String value);
}

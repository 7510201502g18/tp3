package ar.fiuba.tdd.tp1.elements.cell;

import ar.fiuba.tdd.tp1.elements.reference.ReferenceResolver;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;

/**
 * Abstract class for cell references
 * Created by Matias on 08/10/15.
 */
public abstract class CellReference implements ReferenceResolver<Cell> {

    protected ReferenceResolver<Sheet> sheetReference;
    protected Integer row;
    protected String column;

    public CellReference(ReferenceResolver<Sheet> sheetReference, String col, Integer row) {
        this.sheetReference = sheetReference;
        this.row = row;
        this.column = col;
    }

    protected abstract Cell solveCell();

    @Override
    public String toString() {
        return sheetReference.toString().concat("!").concat(this.column).concat(this.row.toString());
    }

    @Override
    public Cell solve() {
        final Cell cell = solveCell();
        return cell;
    }

}

package ar.fiuba.tdd.tp1.elements.cell;

import ar.fiuba.tdd.tp1.elements.formatter.Formatter;
import ar.fiuba.tdd.tp1.elements.formatter.IdentityFormatter;
import ar.fiuba.tdd.tp1.formulas.formulacreator.FormulaCreator;

/**
 * Cells representation.
 * Created by matias on 9/19/15.
 */
public class Cell {

    private Formula formula;
    private Integer row;
    private String column;
    private Formatter formatter;

    public Cell(String col, Integer row) {
        this.formula = FormulaCreator.emptyFormula();
        this.formatter = new IdentityFormatter();
        this.column = col;
        this.row = row;
    }

    public String getValue() {
        return this.formula.evaluate();
    }

    public String getFormattedValue() {
        return this.formatter.format(this.getValue());
    }

    public Formula getFormula() {
        return this.formula;
    }

    public void setFormula(Formula newFormula) {
        this.formula = newFormula;
    }

    @Override
    public String toString() {
        return formula.toString();
    }

    public Integer getRow() {
        return row;
    }

    public String getColumn() {
        return column;
    }

    public String getId() {
        return column + row.toString();
    }

    public Formatter getFormatter() {
        return formatter;
    }

    public void setFormatter(Formatter formatter) {
        this.formatter = formatter;
    }
}

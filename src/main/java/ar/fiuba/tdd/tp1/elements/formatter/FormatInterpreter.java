package ar.fiuba.tdd.tp1.elements.formatter;

/**
 * Created by jonathan on 18/10/15.
 */
public interface FormatInterpreter {
    Formatter interpret(String format);
}

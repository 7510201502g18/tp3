package ar.fiuba.tdd.tp1.elements.printer;

/**
 * Created by jonathan on 15/10/15.
 */
public class PrintException extends RuntimeException {

    public PrintException(String message) {
        super(message);
    }
}

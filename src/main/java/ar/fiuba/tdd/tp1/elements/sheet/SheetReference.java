package ar.fiuba.tdd.tp1.elements.sheet;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.reference.ReferenceResolver;

/**
 * Find the sheet that is request
 * Created by jonathan on 26/09/15.
 */
public class SheetReference implements ReferenceResolver<Sheet> {

    private String sheetId;
    private Book book;

    public SheetReference(String sheetId, Book book) {
        this.sheetId = sheetId;
        this.book = book;
    }

    @Override
    public Sheet solve() {
        return book.getSheet(this.sheetId);
    }

    @Override
    public String toString() {
        return sheetId;
    }

}

package ar.fiuba.tdd.tp1.elements.cell;

import ar.fiuba.tdd.tp1.elements.reference.ColumnComparator;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Created by jonathan on 11/10/15.
 */
public class CellIdIterator implements Iterator<String> {

    private static final char LAST_CHAR = 'Z';
    private String startingCol;
    private String endingCol;
    private String currentCol;
    private Integer currentRow;
    private Integer endingRow;
    private String prevCol;
    private Integer prevRow;

    public CellIdIterator(CellRange range) {
        this.startingCol = range.getStartingCol();
        this.endingCol = range.getEndingCol();
        this.currentCol = range.getStartingCol();
        this.endingRow = range.getEndingRow();
        this.currentRow = range.getStartingRow();
        prevCol = this.currentCol;
        prevRow = this.currentRow;
    }

    public CellIdIterator(String startingCol, Integer startingRow, Integer cols, Integer rows) {
        this(new CellRange(startingCol, startingRow, rollCols(startingCol, cols - 1), startingRow + rows - 1));
    }

    public static String rollCols(String col, Integer count) {
        for (int i = 0; i < count; i++) {
            col = nextCol(col, col.length() - 1);
        }
        return col;
    }

    public static Integer rollRow(Integer row, Integer count) {
        return row + count;
    }

    public static String firstCol() {
        return "A";
    }

    public static Integer firstRow() {
        return 1;
    }

    private static String nextCol(String col, Integer idx) {
        if (idx < 0) {
            return "A" + col;
        }
        char nextChar = col.charAt(idx);
        nextChar++;
        if (nextChar > LAST_CHAR) {
            col = replaceCharAt(col, 'A', idx);
            return nextCol(col, idx - 1);

        } else {
            return replaceCharAt(col, nextChar, idx);
        }
    }

    private static String replaceCharAt(String str, char replacement, Integer idx) {
        StringBuilder builder = new StringBuilder(str);
        builder.setCharAt(idx, replacement);
        return builder.toString();

    }

    @Override
    public boolean hasNext() {
        String col = currentCol;
        Integer colCompare = 0;
        ColumnComparator comparator = new ColumnComparator();
        colCompare = comparator.compare(col, endingCol);

        Boolean cond1 = currentRow < endingRow && colCompare < 0;
        Boolean cond2 = currentRow <= endingRow && colCompare == 0;
        Boolean cond3 = currentRow.equals(endingRow) && colCompare <= 0;
        return cond1 || cond2 || cond3;
    }

    @Override
    public String next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }
        final String id = currentCol + currentRow.toString();
        prevRow = currentRow;
        prevCol = currentCol;
        nextId();
        return id;
    }

    private void nextId() {
        Boolean currentEqLast = currentCol.equals(endingCol);
        currentCol = rollCols(currentCol, 1);
        if (currentEqLast) {
            currentCol = startingCol;
            currentRow = rollRow(currentRow, 1);
        }
    }

    public String getCurrentColumn() {
        return prevCol;
    }

    public Integer getCurrentRow() {
        return prevRow;
    }
}

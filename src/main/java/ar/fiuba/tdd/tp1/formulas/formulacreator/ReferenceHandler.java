package ar.fiuba.tdd.tp1.formulas.formulacreator;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.ReferenceFormula;

import java.util.Stack;

/**
 * Link of the chain that handles reference cells.
 * Created by matias on 9/26/15.
 */
public class ReferenceHandler extends CharHandler {

    private Book activeBook;

    public ReferenceHandler(Stack<Formula> formulaTree, Book activeBook) {
        super(formulaTree);
        this.activeBook = activeBook;
    }

    private static Boolean isCellReference(String stringToCheck) {
        return stringToCheck.matches("^(([^!]|[^!]+)!)?[A-Z]+[0-9]+$");
    }

    @Override
    public void handle(String string) {
        if (isCellReference(string)) {
            getFormulaTree().push(new ReferenceFormula(string, activeBook));
        } else {
            next(string);
        }
    }


}

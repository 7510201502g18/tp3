package ar.fiuba.tdd.tp1.formulas.formulacreator;

import ar.fiuba.tdd.tp1.elements.cell.Formula;
import ar.fiuba.tdd.tp1.formulas.InvalidFormula;

import java.util.Stack;

/**
 * Link of the formula constructor chain that throws exceptions.
 * Created by matias on 9/26/15.
 */
public class OperationNotSupportedHandler extends CharHandler {

    public OperationNotSupportedHandler(Stack<Formula> formulaTree) {
        super(formulaTree);
        setNextLink(null);
    }

    @Override
    public void handle(String string) {
        getFormulaTree().push(new InvalidFormula("Operation in formula not supported."));
    }
}

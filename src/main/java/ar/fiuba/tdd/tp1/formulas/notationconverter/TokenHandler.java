package ar.fiuba.tdd.tp1.formulas.notationconverter;

import java.util.Deque;

/**
 * Defines the methods that must have the handlers
 * Created by jonathan on 26/09/15.
 */
public abstract class TokenHandler {
    private StringBuilder output;
    private Deque<String> stack;
    private TokenHandler next;

    public TokenHandler(StringBuilder output, Deque<String> stack) {
        this.output = output;
        this.stack = stack;
    }

    public abstract void handle(String token);

    public StringBuilder getOutput() {
        return output;
    }

    public Deque<String> getStack() {
        return stack;
    }

    public void setNext(TokenHandler next) {
        this.next = next;
    }

    protected void next(String token) {
        if (next != null) {
            next.handle(token);
        }
    }
}

package ar.fiuba.tdd.tp1.formulas.notationconverter;

import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

/**
 * Converts from infix to postfix
 * Created by jonathan on 26/09/15.
 */
public class OperationHandler extends TokenHandler {


    private static Map<String, Operator> ops = new HashMap<String, Operator>() {
        {
            put("+", Operator.ADD);
            put("-", Operator.SUBTRACT);
            put("*", Operator.MULTIPLY);
            put("/", Operator.DIVIDE);
        }
    };

    public OperationHandler(StringBuilder output, Deque<String> stack) {
        super(output, stack);
        this.setNext(new LeftParenthesisHandler(output, stack));
    }

    @Override
    public void handle(String token) {
        if (ops.containsKey(token)) {
            while (!getStack().isEmpty() && isHigerPrec(token, getStack().peek())) {
                getOutput().append(getStack().pop()).append(' ');
            }
            getStack().push(token);
        } else {
            next(token);
        }

    }

    private boolean isHigerPrec(String op, String sub) {
        return (ops.containsKey(sub) && ops.get(sub).precedence >= ops.get(op).precedence);
    }

    private enum Operator {
        ADD(1), SUBTRACT(2), MULTIPLY(3), DIVIDE(4);
        final int precedence;

        Operator(int precedenceNumber) {
            precedence = precedenceNumber;
        }
    }
}

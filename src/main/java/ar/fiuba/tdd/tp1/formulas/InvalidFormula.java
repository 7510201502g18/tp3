package ar.fiuba.tdd.tp1.formulas;

import ar.fiuba.tdd.tp1.elements.cell.Formula;

/**
 * Formula created when the string representing the formula is invalid.
 * Created by matias on 9/27/15.
 */
public class InvalidFormula implements Formula {

    private String message;

    public InvalidFormula(String message) {
        this.message = message;
    }

    @Override
    public String evaluate() {
        throw new InvalidFormulaException(this.message);
    }
}

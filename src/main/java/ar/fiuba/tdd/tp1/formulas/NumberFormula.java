package ar.fiuba.tdd.tp1.formulas;

import ar.fiuba.tdd.tp1.elements.cell.Formula;

/**
 * Formula created when the string representing the formula is a number
 * Created by matias on 9/24/15.
 */
public class
        NumberFormula implements Formula {

    private Float numberValue;

    public NumberFormula(Float numberValue) {
        this.numberValue = numberValue;
    }

    @Override
    public String evaluate() {
        return this.numberValue.toString();
    }

    @Override
    public String toString() {
        return this.numberValue.toString();
    }

    @Override
    public String toStringAsRootFormula() {
        return this.toString();
    }
}

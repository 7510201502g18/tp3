package ar.fiuba.tdd.tp1.userinterface.bookoptions;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.userinterface.DynamicCompositeMenuOption;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jonathan on 22/10/15.
 */
public class ChangeActiveBookOption extends DynamicCompositeMenuOption {

    private SpreadSheet spreadSheet;

    public ChangeActiveBookOption(SpreadSheet spreadSheet) {
        super("Select Available Books");
        this.spreadSheet = spreadSheet;
    }

    @Override
    protected List<MenuOption> createOptions() {
        List<MenuOption> options = new ArrayList<>();
        final List<String> books = spreadSheet.getBooks();
        for (String book : books) {
            options.add(new BookOption(book, spreadSheet));
        }
        return options;
    }
}

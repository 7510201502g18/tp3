package ar.fiuba.tdd.tp1.userinterface.sheetoptions;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.files.FileOperations;
import ar.fiuba.tdd.tp1.files.ReadException;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class ImportSheetOption extends MenuOption {

    private static final String SHEET_INPUT = "Please enter sheet name to import";
    private static final String FILE_INPUT = "Please enter csv file";
    private String sheetId;
    private SpreadSheet spreadSheet;

    public ImportSheetOption(SpreadSheet spreadSheet) {
        super("Import new sheet", SHEET_INPUT, true);
        this.spreadSheet = spreadSheet;
        sheetId = null;
    }


    @Override
    public MenuOption execute(String input) {
        if (sheetId == null) {
            if (input.equals("")) {
                lastError = "Please enter a valid name";
            } else {
                lastError = "";
                sheetId = input;
                inputMessage = FILE_INPUT;
            }
            return this;
        }
        return super.execute(input);
    }

    @Override
    protected String executeSafeErrors(String input) {
        try {
            FileOperations.importSheet(input, spreadSheet.getActiveBook(), sheetId, spreadSheet);
        } catch (ReadException e) {
            return e.getMessage();
        }
        return "";

    }

    @Override
    public void reset() {
        super.reset();
        inputMessage = SHEET_INPUT;
        sheetId = null;
    }
}

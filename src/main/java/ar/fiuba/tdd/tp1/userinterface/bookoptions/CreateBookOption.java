package ar.fiuba.tdd.tp1.userinterface.bookoptions;

import ar.fiuba.tdd.tp1.api.AlreadyExistentBookException;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class CreateBookOption extends MenuOption {

    private SpreadSheet spreadSheet;

    public CreateBookOption(SpreadSheet spreadSheet) {
        super("Create Book", "Enter new Book name", true);
        this.spreadSheet = spreadSheet;
    }


    @Override
    protected String executeSafeErrors(String input) {
        try {
            spreadSheet.createBookWithDefaultSheet(input);
        } catch (AlreadyExistentBookException e) {
            return "Book \"" + input + "\" already exists";
        }
        return "";
    }

    @Override
    public Boolean validateInput(String input) {
        return input.length() > 0;
    }
}

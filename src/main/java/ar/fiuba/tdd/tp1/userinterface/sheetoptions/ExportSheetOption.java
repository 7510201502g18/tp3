package ar.fiuba.tdd.tp1.userinterface.sheetoptions;


import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.elements.printer.PrintException;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.queries.Query;
import ar.fiuba.tdd.tp1.queries.factory.QueryFactory;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class ExportSheetOption extends MenuOption {

    private SpreadSheet spreadSheet;

    public ExportSheetOption(SpreadSheet spreadSheet) {
        super("Export Active Sheet", "Please enter filename", true);
        this.spreadSheet = spreadSheet;
    }

    @Override
    protected String executeSafeErrors(String input) {
        final Query<Printer> query = QueryFactory.printQuery().activeSheetToCSV(input);
        try {
            spreadSheet.execute(query).print();
        } catch (PrintException e) {
            return e.getMessage();
        }
        return "";
    }
}

package ar.fiuba.tdd.tp1.userinterface.miscoptions;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class UndoOption extends MenuOption {

    private SpreadSheet spreadSheet;

    public UndoOption(SpreadSheet spreadSheet) {
        super("Undo", "", false);
        this.spreadSheet = spreadSheet;
    }

    @Override
    protected String executeSafeErrors(String input) {
        spreadSheet.undo();
        return "";
    }
}

package ar.fiuba.tdd.tp1.userinterface;

import ar.fiuba.tdd.tp1.api.SpreadSheet;


/**
 * Created by maxi on 18/10/15.
 */
public class UserInterface {

    private Menu menu;
    private ContentPrinter printer;

    public UserInterface(SpreadSheet spreadSheet) {
        printer = new ContentPrinter(spreadSheet);
        menu = new Menu(MenuFactory.createRootMenu(printer, spreadSheet));
    }

    public static void main(String[] args) {
        final SpreadSheet spreadSheet = new SpreadSheet();
        spreadSheet.createBookWithDefaultSheet("Book 1");

        UserInterface userInterface = new UserInterface(spreadSheet);
        userInterface.start();
    }

    private static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");

            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (final Exception e) {
            throw new RuntimeException("Error cleaning console");
        }
    }

    public void start() {

        while (menu.shouldContinue()) {
            clearConsole();
            printer.print();
            this.menu.execute();
        }

    }
}
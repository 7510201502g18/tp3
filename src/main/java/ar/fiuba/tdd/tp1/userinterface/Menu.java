package ar.fiuba.tdd.tp1.userinterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

/**
 * Created by jonathan on 21/10/15.
 */
public class Menu {


    private MenuOption root;
    private MenuOption actual;

    public Menu(MenuOption root) {
        this.root = root;
        this.actual = root;
    }

    public static String getInput() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));
            try {
                return bufferedReader.readLine();
            } catch (IOException e) {
                throw new UserInterfaceException("Cannot read user input");
            }
        } catch (UnsupportedEncodingException e) {
            throw new UserInterfaceException("Cannot suport encoding");
        }
    }

    public void execute() {
        if (!shouldContinue()) {
            throw new UserInterfaceException("End interface is required");
        }
        actual.printTitle();
        System.out.println();
        actual.printInputMessage();
        final String input = getInput();
        if (actual.validateInput(input)) {
            final MenuOption prev = actual;
            actual = actual.execute(input);

            if (actual == null) {
                handleBack(prev);
            } else {
                handleNext(prev);
            }
        }


    }

    private void handleNext(MenuOption prev) {
        if (!actual.equals(prev)) {
            prev.reset();
        }
        if (!actual.getNeedInput()) {
            actual.execute("");
            actual = root;
        }
    }

    private void handleBack(MenuOption prev) {
        if (!prev.equals(root)) {
            actual = root;
            actual.reset();

        }
    }

    public Boolean shouldContinue() {
        return actual != null;
    }

}


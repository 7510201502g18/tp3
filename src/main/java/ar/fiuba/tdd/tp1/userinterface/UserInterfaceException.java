package ar.fiuba.tdd.tp1.userinterface;

/**
 * Created by maxi on 18/10/15.
 */
public class UserInterfaceException extends RuntimeException {

    public UserInterfaceException(String message) {
        super(message);
    }
}
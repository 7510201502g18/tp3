package ar.fiuba.tdd.tp1.userinterface.bookoptions;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class BookOption extends MenuOption {

    private String book;
    private SpreadSheet spreadsheet;

    public BookOption(String book, SpreadSheet spreadSheet) {
        super(book, "", false);
        this.book = book;
        this.spreadsheet = spreadSheet;
    }

    @Override
    protected String executeSafeErrors(String input) {
        spreadsheet.setActiveBook(book);
        return "";
    }
}

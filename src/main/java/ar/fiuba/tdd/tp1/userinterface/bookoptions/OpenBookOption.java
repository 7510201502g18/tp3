package ar.fiuba.tdd.tp1.userinterface.bookoptions;

import ar.fiuba.tdd.tp1.api.AlreadyExistentBookException;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.files.FileOperations;
import ar.fiuba.tdd.tp1.files.ReadException;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

/**
 * Created by jonathan on 22/10/15.
 */
public class OpenBookOption extends MenuOption {

    private SpreadSheet spreadSheet;

    public OpenBookOption(SpreadSheet spreadSheet) {
        super("Open Book", "Please enter file name", true);
        this.spreadSheet = spreadSheet;
    }

    @Override
    protected String executeSafeErrors(String input) {
        try {
            FileOperations.openBook(input, spreadSheet);
        } catch (ReadException e) {
            return e.getMessage();
        } catch (AlreadyExistentBookException e) {
            return "Already exists the book " + input;
        }
        return "";
    }
}

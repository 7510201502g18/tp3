package ar.fiuba.tdd.tp1.userinterface;

/**
 * Created by jonathan on 21/10/15.
 */
public abstract class MenuOption {

    protected String inputMessage;
    protected String lastError;
    protected Boolean needInput;
    private String title;

    public MenuOption(String title, String inputMessage, Boolean needInput) {
        this.title = title;
        this.inputMessage = inputMessage;
        lastError = "";
        this.needInput = needInput;
    }

    public void printTitle() {
        System.out.println(title);
    }

    public void printInputMessage() {
        String message = inputMessage;
        if (!"".equals(lastError)) {
            message = lastError + ". " + message;
        }
        System.out.println(message);
    }

    public Boolean validateInput(String input) {
        return true;
    }

    public String getTitle() {
        return title;
    }

    public String getInputMessage() {
        return inputMessage;
    }

    public MenuOption execute(String input) {
        final String result = executeSafeErrors(input);
        lastError = result;
        return lastError.equals("") ? null : this;
    }

    public void reset() {
        lastError = "";
    }

    protected abstract String executeSafeErrors(String input);


    public Boolean getNeedInput() {
        return needInput;
    }
}

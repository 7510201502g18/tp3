package ar.fiuba.tdd.tp1.userinterface;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.queries.Query;
import ar.fiuba.tdd.tp1.queries.factory.QueryFactory;

/**
 * Created by jonathan on 22/10/15.
 */
public class ContentPrinter {

    private String col;
    private Integer row;
    private SpreadSheet spreadSheet;

    public ContentPrinter(SpreadSheet spreadSheet) {
        this.spreadSheet = spreadSheet;
        this.col = "A";
        this.row = 1;
    }

    public void setCol(String col) {
        this.col = col;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public void print() {
        final Query<Printer> query = QueryFactory.printQuery().activeSheetToConsole(this.col, this.row);
        spreadSheet.execute(query).print();
        System.out.println();
        System.out.println();
        System.out.println();
    }
}

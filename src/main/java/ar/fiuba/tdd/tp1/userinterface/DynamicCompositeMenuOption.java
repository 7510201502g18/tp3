package ar.fiuba.tdd.tp1.userinterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jonathan on 22/10/15.
 */
public abstract class DynamicCompositeMenuOption extends CompositeMenuOption {


    public DynamicCompositeMenuOption(String title) {
        super(title, new ArrayList<>());
    }

    protected abstract List<MenuOption> createOptions();

    @Override
    public void printTitle() {
        this.options = createOptions();
        super.printTitle();
    }
}

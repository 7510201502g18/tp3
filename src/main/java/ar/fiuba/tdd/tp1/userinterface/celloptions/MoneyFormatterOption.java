package ar.fiuba.tdd.tp1.userinterface.celloptions;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.elements.formatter.Formatter;
import ar.fiuba.tdd.tp1.formatter.CurrencySymbol;
import ar.fiuba.tdd.tp1.formatter.MoneyFormatter;

import java.util.regex.Pattern;


/**
 * Created by maxi on 20/10/15.
 */
public class MoneyFormatterOption extends CellOption {

    private CurrencySymbol symbol;

    public MoneyFormatterOption(CurrencySymbol symbol, SpreadSheet spreadSheet) {
        super("Money Formatter " + symbol.getSymbol(), "Please select decimal digits (0 or 2)", true, spreadSheet);
        this.symbol = symbol;
    }


    @Override
    protected String executeSafeErrors(String input) {
        Formatter formatter = getFormatter(input);
        if (formatter == null) {
            return "Invalid decimals count";
        }
        Action action = ActionFactory.cellAction(col, row, spreadSheet.getActiveSheetId()).changeFormatter(formatter);
        spreadSheet.execute(action);
        return "";
    }

    private Formatter getFormatter(String input) {
        Formatter formatter = null;
        if (input.equals("0")) {
            formatter = MoneyFormatter.noDecimalsFormatter(symbol);

        }
        if (input.equals("2")) {
            formatter = MoneyFormatter.twoDecimalsFormatter(symbol);
        }
        return formatter;
    }


    @Override
    protected Boolean validateCellInput(String input) {
        return Pattern.compile("^[0-9]+$").matcher(input).matches();
    }

}

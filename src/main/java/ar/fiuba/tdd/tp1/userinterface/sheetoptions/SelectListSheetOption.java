package ar.fiuba.tdd.tp1.userinterface.sheetoptions;

import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.userinterface.DynamicCompositeMenuOption;
import ar.fiuba.tdd.tp1.userinterface.MenuOption;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jonathan on 22/10/15.
 */
public class SelectListSheetOption extends DynamicCompositeMenuOption {


    private SpreadSheet spreadSheet;

    public SelectListSheetOption(SpreadSheet spreadSheet) {
        super("Select a sheet");
        this.spreadSheet = spreadSheet;
    }


    @Override
    protected List<MenuOption> createOptions() {
        final List<String> sheetsInBook = spreadSheet.getSheetsInBook(spreadSheet.getActiveBook());
        List<MenuOption> options = new ArrayList<>();
        for (Iterator<String> iterator = sheetsInBook.iterator(); iterator.hasNext(); ) {
            String next = iterator.next();
            options.add(new SelectSheetOption(next, spreadSheet));
        }
        return options;
    }
}

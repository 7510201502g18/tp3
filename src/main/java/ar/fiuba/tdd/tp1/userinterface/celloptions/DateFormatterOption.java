package ar.fiuba.tdd.tp1.userinterface.celloptions;

import ar.fiuba.tdd.tp1.actions.Action;
import ar.fiuba.tdd.tp1.actions.factory.ActionFactory;
import ar.fiuba.tdd.tp1.api.SpreadSheet;
import ar.fiuba.tdd.tp1.formatter.DateFormat;
import ar.fiuba.tdd.tp1.formatter.DateFormatter;

/**
 * Created by maxi on 20/10/15.
 */
public class DateFormatterOption extends CellOption {

    private DateFormat format;

    public DateFormatterOption(DateFormat format, SpreadSheet spreadSheet) {
        super("Date Format : " + format.toString(), "", false, spreadSheet);
        this.format = format;
    }


    @Override
    protected String executeSafeErrors(String input) {
        DateFormatter ff = new DateFormatter(format);
        final Action action = ActionFactory.cellAction(this.col, this.row, spreadSheet.getActiveSheetId()).changeFormatter(ff);
        spreadSheet.execute(action);
        return "";
    }

}

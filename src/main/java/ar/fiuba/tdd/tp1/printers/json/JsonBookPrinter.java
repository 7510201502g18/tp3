package ar.fiuba.tdd.tp1.printers.json;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.printer.BookPrinter;
import ar.fiuba.tdd.tp1.elements.printer.PrintException;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;
import ar.fiuba.tdd.tp1.printers.json.model.JsonBook;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by maxi on 12/10/15.
 */
public class JsonBookPrinter extends BookPrinter {

    private ObjectMapper mapper;
    private String fileName;
    private JsonBook json;

    public JsonBookPrinter(Book book, String name) {
        super(book);
        this.fileName = name;
        this.mapper = new ObjectMapper();

    }


    @Override
    protected Printer getSheetPrinter(Sheet sheet) {
        return new JsonSheetPrinter(sheet, json.getCells());
    }

    @Override
    protected void printHeader(Book book) {
        json = new JsonBook(book.getName());
        json.setCells(new ArrayList<>());
    }

    @Override
    protected void printSheetSeparator() {

    }

    @Override
    protected void endPrint() {
        try (PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(new File(fileName + ".jss")),
                "UTF-8"))) {
            mapper.writeValue(pw, json);
        } catch (IOException e) {
            throw new PrintException("Cannot write file");
        }
    }

}
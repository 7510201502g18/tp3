package ar.fiuba.tdd.tp1.printers.json.model;

/**
 * Created by jonathan on 16/10/15.
 */
public class JsonFormatter {

    private String type;
    private String format;

    public JsonFormatter() {
    }

    public JsonFormatter(String type, String format) {
        this.type = type;
        this.format = format;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }
}

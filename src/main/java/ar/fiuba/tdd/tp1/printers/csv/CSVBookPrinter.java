package ar.fiuba.tdd.tp1.printers.csv;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.printer.BookPrinter;
import ar.fiuba.tdd.tp1.elements.printer.PrintException;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;

import java.io.*;
import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by maxi on 11/10/15.
 */
public class CSVBookPrinter extends BookPrinter {

    private Writer fileWriter;

    public CSVBookPrinter(Book book, String fileName) {
        super(book);
        String pathname = fileName + ".csv";
        try {
            Writer outputWriter = new OutputStreamWriter(new FileOutputStream(new File(pathname)), "UTF-8");
            fileWriter = new PrintWriter(outputWriter);
        } catch (IOException e) {
            throw new PrintException("Cannot print to file");
        }

    }

    @Override
    protected void printHeader(Book book) {

    }

    @Override
    protected Printer getSheetPrinter(Sheet sheet) {
        return new CSVSheetPrinter(sheet, this.fileWriter);
    }

    @Override
    protected void printSheetSeparator() {

    }

    @Override
    protected void endPrint() {
        try {
            fileWriter.close();
        } catch (IOException e) {
            throw new PrintException("Cannot print to file");
        }
    }


    @Override
    protected Iterator<Sheet> sheetIterator() {
        Sheet activeSheet = book.getActiveSheet();
        return Arrays.asList(activeSheet).iterator();
    }
}
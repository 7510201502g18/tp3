package ar.fiuba.tdd.tp1.printers.console;

import ar.fiuba.tdd.tp1.elements.book.Book;
import ar.fiuba.tdd.tp1.elements.printer.BookPrinter;
import ar.fiuba.tdd.tp1.elements.printer.Printer;
import ar.fiuba.tdd.tp1.elements.sheet.Sheet;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by maxi on 12/10/15.
 */
public class ConsoleBookPrinter extends BookPrinter {

    private Integer startingRow;
    private String startingCol;


    public ConsoleBookPrinter(Book book, String startingCol, Integer startingRow) {
        super(book);
        setStartingParams(startingCol, startingRow);
    }

    private void setStartingParams(String startingCol, Integer startingRow) {
        this.startingCol = startingCol;
        this.startingRow = startingRow;
    }

    @Override
    protected void printHeader(Book book) {
        System.out.println("Book: " + book.getName());
    }

    @Override
    protected Printer getSheetPrinter(Sheet sheet) {
        return new ConsoleSheetPrinter(sheet, startingCol, startingRow);
    }

    @Override
    protected void printSheetSeparator() {

    }

    @Override
    protected Iterator<Sheet> sheetIterator() {
        Sheet activeSheet = book.getActiveSheet();
        return Arrays.asList(activeSheet).iterator();
    }

    @Override
    protected void endPrint() {
        System.out.println();
    }

}

package ar.fiuba.tdd.tp1.printers.json.model;

/**
 * Created by jonathan on 16/10/15.
 */
public class JsonCell {

    private String sheet;
    private String id;
    private String value;
    private JsonFormatter formatter;

    public JsonCell() {
    }

    public JsonCell(String sheet, String id, String value, JsonFormatter formatter) {
        this.sheet = sheet;
        this.formatter = formatter;
        this.id = id;
        this.value = value;
    }

    public void setFormatter(JsonFormatter formatter) {
        this.formatter = formatter;
    }


    public void setSheet(String sheet) {
        this.sheet = sheet;
    }

    public String getId() {
        return id;
    }

    public String getSheet() {
        return sheet;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public JsonFormatter getFormatter() {
        return formatter;
    }

    public String getValue() {
        return value;
    }
}

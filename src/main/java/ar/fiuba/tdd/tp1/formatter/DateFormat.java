package ar.fiuba.tdd.tp1.formatter;

/**
 * Created by jonathan on 15/10/15.
 */
public enum DateFormat {

    YYYYMMDD("yyyy-MM-dd"),
    DDMMYYYY("dd-MM-yyyy"),
    MMDDYYYY("MM-dd-yyyy"),
    YYYYMMDDTHHMMSSMSSS("yyyy-MM-dd'T'HH:mm:ss");
    private String pattern;

    DateFormat(String patter) {
        this.pattern = patter;
    }

    public String getPattern() {
        return pattern;
    }

    public static DateFormat getByPattern(String pattern) {
        for (DateFormat c : values()) {
            if (c.pattern.equals(pattern) || c.pattern.toUpperCase().equals(pattern)) {
                return c;
            }
        }
        throw new IllegalArgumentException(String.valueOf(pattern));
    }
}

package ar.fiuba.tdd.tp1.formatter;

import ar.fiuba.tdd.tp1.elements.formatter.FormatException;
import ar.fiuba.tdd.tp1.elements.formatter.FormatInterpreter;
import ar.fiuba.tdd.tp1.elements.formatter.Formatter;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jonathan on 14/10/15.
 */
public class NumberFormatter implements Formatter {

    public static final String TYPE = "NUMBER";

    private int decimals;

    public NumberFormatter(int decimals) {
        if (decimals < 0) {
            throw new IllegalArgumentException();
        }
        this.decimals = decimals;
    }

    public static FormatInterpreter interpreter() {
        return (String format) -> {
            Matcher matcher = Pattern.compile("^[0-9]+$").matcher(format);
            if (matcher.matches()) {
                return new NumberFormatter(Integer.valueOf(format));
            } else {
                throw new FormatException("Invalid number format");
            }
        };
    }

    @Override
    public String format(String value) {
        try {

            Float intValue = Float.valueOf(value);
            DecimalFormat fm = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);
            fm.setMaximumFractionDigits(decimals);
            fm.setMinimumFractionDigits(decimals);
            return fm.format(intValue);
        } catch (NumberFormatException e) {
            throw new FormatException("Cannot convert to number");
        }
    }

    @Override
    public String getType() {
        return NumberFormatter.TYPE;
    }

    @Override
    public String getFormat() {
        return String.valueOf(decimals);
    }

    @Override
    public void changeProperty(String property, String value) {
        if ("decimal".equals(property)) {
            decimals = Integer.parseInt(value);
        } else {
            throw new IllegalArgumentException();

        }
    }
}

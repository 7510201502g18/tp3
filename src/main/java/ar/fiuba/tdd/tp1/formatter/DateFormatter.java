package ar.fiuba.tdd.tp1.formatter;

import ar.fiuba.tdd.tp1.elements.formatter.FormatException;
import ar.fiuba.tdd.tp1.elements.formatter.FormatInterpreter;
import ar.fiuba.tdd.tp1.elements.formatter.Formatter;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by jonathan on 14/10/15.
 */
public class DateFormatter implements Formatter {

    public static final String TYPE = "DATE";
    public static final String BAD_DATE = "BAD_DATE";
    private List<String> posiblePatterns;
    private DateFormat pattern;

    public DateFormatter(DateFormat df) {
        this.pattern = df;
        posiblePatterns = new ArrayList<>();
        for (DateFormat f : DateFormat.values()) {
            posiblePatterns.add(f.getPattern());
        }
    }

    public static FormatInterpreter interpreter() {
        return (String format) -> {
            try {
                DateFormat dateFormat = DateFormat.valueOf(format);
                return new DateFormatter(dateFormat);

            } catch (IllegalArgumentException e) {
                throw new FormatException("Invalid date format: " + format);
            }
        };
    }

    @Override
    public String format(String value) {
        try {
            LocalDate date = convertToDate(value);
            if (date == null) {
                throw new FormatException(BAD_DATE);
            }
            DateTimeFormatter finalFormatter = DateTimeFormatter.ofPattern(pattern.getPattern());
            return finalFormatter.format(date);
        } catch (DateTimeException e) {
            throw new FormatException(BAD_DATE);
        }
    }

    private LocalDate parse(String value, String pattern) {
        DateTimeFormatter interpr = DateTimeFormatter.ofPattern(pattern);
        try {
            return LocalDate.parse(value, interpr);
        } catch (DateTimeException e) {
            return null;
        }

    }

    private LocalDate convertToDate(String value) {
        Iterator<String> iterator = posiblePatterns.iterator();
        LocalDate date = null;
        while (date == null && iterator.hasNext()) {
            date = parse(value, iterator.next());
        }
        return date;
    }

    @Override
    public String getType() {
        return DateFormatter.TYPE;
    }

    @Override
    public String getFormat() {
        return pattern.toString();
    }

    @Override
    public void changeProperty(String property, String value) {
        if ("format".equals(property)) {
            pattern = DateFormat.getByPattern(value);
        } else {

            throw new IllegalArgumentException();
        }
    }
}

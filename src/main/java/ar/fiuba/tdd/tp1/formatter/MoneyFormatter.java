package ar.fiuba.tdd.tp1.formatter;

import ar.fiuba.tdd.tp1.elements.formatter.FormatException;
import ar.fiuba.tdd.tp1.elements.formatter.FormatInterpreter;
import ar.fiuba.tdd.tp1.elements.formatter.Formatter;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jonathan on 14/10/15.
 */
public class MoneyFormatter implements Formatter {

    public static final String TYPE = "MONEY";
    private static final Pattern FORMAT_PATTERN = Pattern.compile("^(.+)!(.+)");
    private CurrencySymbol symbol;
    private Formatter numberFormatter;

    private MoneyFormatter(CurrencySymbol symbol, int decimal) {
        this(symbol, new NumberFormatter(decimal));
    }

    private MoneyFormatter(CurrencySymbol symbol, Formatter formatter) {
        this.numberFormatter = formatter;
        this.symbol = symbol;

    }

    public static MoneyFormatter twoDecimalsFormatter(CurrencySymbol symbol) {
        return new MoneyFormatter(symbol, 2);
    }

    public static MoneyFormatter noDecimalsFormatter(CurrencySymbol symbol) {
        return new MoneyFormatter(symbol, 0);
    }

    public static FormatInterpreter interpreter() {
        return (String format) -> {
            Matcher matcher = FORMAT_PATTERN.matcher(format);
            if (matcher.matches()) {
                String symbol = matcher.group(1);
                CurrencySymbol currencySymbol = CurrencySymbol.valueOf(symbol);
                FormatInterpreter numberint = NumberFormatter.interpreter();
                Formatter interpret = numberint.interpret(matcher.group(2));
                return new MoneyFormatter(currencySymbol, interpret);
            }
            throw new FormatException("Wrong money format");

        };
    }

    @Override
    public String format(String value) {
        try {
            return symbol.getSymbol() + " " + numberFormatter.format(value);

        } catch (FormatException e) {
            throw new FormatException("BAD_CURRENCY");
        }
    }

    @Override
    public String getType() {
        return MoneyFormatter.TYPE;
    }

    @Override
    public String getFormat() {
        return symbol + "!" + numberFormatter.getFormat();
    }

    @Override
    public void changeProperty(String property, String value) {
        if ("symbol".equals(property)) {
            final CurrencySymbol bySymbol = CurrencySymbol.getBySymbol(value);
            this.symbol = bySymbol;
        } else {

            Integer val = Integer.valueOf(value);
            if (!val.equals(0) && !val.equals(2)) {
                throw new IllegalArgumentException();
            }
            this.numberFormatter.changeProperty(property, value);
        }
    }
}
